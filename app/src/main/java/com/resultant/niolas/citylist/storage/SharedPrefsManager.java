package com.resultant.niolas.citylist.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.resultant.niolas.citylist.core.di.AppComponent;
import com.resultant.niolas.citylist.core.di.DaggerService;

import javax.inject.Inject;

/**
 * Created by lauraldo on 08.01.17.
 */

public class SharedPrefsManager {

    private static final String SEL_CITY_KEY = "SEL_CITY_KEY";

    private SharedPreferences sharedPreferences;

    @Inject
    Context context;

    @Inject
    public SharedPrefsManager() {
        DaggerService.getComponent(AppComponent.class).inject(this);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    void saveSelectedCity(int cityId) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(SEL_CITY_KEY, cityId);
        editor.apply();
    }

    int loadSelectedCity() {
        return sharedPreferences.getInt(SEL_CITY_KEY, -1);
    }

}
