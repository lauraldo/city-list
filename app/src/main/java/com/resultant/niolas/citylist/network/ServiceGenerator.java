package com.resultant.niolas.citylist.network;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.squareup.moshi.Moshi;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

/**
 * Created by lauraldo on 08.01.17.
 */

public class ServiceGenerator {

    public static final String API_BASE_URL = "https://atw-backend.azurewebsites.net/api/";

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static Moshi moshi = new Moshi.Builder()
            .build();

    static {
        /*HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(logging);*/
    }

    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(API_BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .client(httpClient.build());

    public static <S> S createService(Class<S> serviceClass) {
        Retrofit retrofit = builder.build();
        return retrofit.create(serviceClass);
    }

    public static Moshi getMoshi() {
        return moshi;
    }

}
