package com.resultant.niolas.citylist.ui.list;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.resultant.niolas.citylist.R;
import com.resultant.niolas.citylist.databinding.FragmentRefreshableListBinding;

/**
 * Created by lauraldo on 09.01.17.
 */

public class CountryListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private FragmentRefreshableListBinding binding;

    private LinearLayoutManager layoutManager;

    //region ================================== Lifecycle ==================================

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentRefreshableListBinding.inflate(inflater, container, false);
        binding.swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        binding.swipeRefreshLayout.setOnRefreshListener(this);
        layoutManager = new LinearLayoutManager(getMainActivity());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getMainActivity().setCitiesAttached(true);
    }

    @Override
    public void onDestroyView() {
        getMainActivity().setCitiesAttached(false);
        super.onDestroyView();
    }

    //endregion

    private MainActivity getMainActivity() {
        if (!(getActivity() instanceof MainActivity)) {
            throw new IllegalStateException("Parent activity should be MainActivity");
        }
        return (MainActivity) getActivity();
    }

    public void setAdapter(CountriesAdapter adapter) {
        binding.cityList.setLayoutManager(layoutManager);
        binding.cityList.addItemDecoration(new DividerItemDecoration(getMainActivity(),
                DividerItemDecoration.VERTICAL));
        binding.cityList.setAdapter(adapter);
    }

    public CountriesAdapter getAdapter() {
        return (CountriesAdapter) binding.cityList.getAdapter();
    }

    //region ================================== SwipeRefreshLayout ==================================

    @Override
    public void onRefresh() {
        getMainActivity().swipeRefreshPerformed();
    }

    public void setRefreshing(boolean refreshing) {
        binding.swipeRefreshLayout.setRefreshing(refreshing);
    }

    //endregion
}
