package com.resultant.niolas.citylist.ui.splash;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.WindowManager;

import com.resultant.niolas.citylist.R;
import com.resultant.niolas.citylist.core.di.DaggerService;
import com.resultant.niolas.citylist.core.view.BaseActivity;
import com.resultant.niolas.citylist.databinding.ActivitySplashBinding;
import com.resultant.niolas.citylist.ui.list.MainActivity;

import javax.inject.Inject;

import dagger.Provides;

public class SplashActivity extends BaseActivity implements ISplashView {

    private ActivitySplashBinding binding;

    @Inject
    SplashPresenter presenter;

    //region ================================== Lifecycle ==================================

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setBinding(DataBindingUtil.setContentView(this, R.layout.activity_splash));
        binding = (ActivitySplashBinding) getBinding();

        DaggerService.getComponent(Component.class, new Module()).inject(this);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.attachView(this);
        presenter.initView();
    }

    @Override
    protected void onStop() {
        presenter.detachView();
        super.onStop();
    }

    //endregion

    //region ================================== ISplashView ==================================

    @Override
    public void proceedToMainScreen() {
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
        DaggerService.killScope(PrecacheScope.class);
        finish();
    }

    //endregion

    //region ================================== DI ==================================

    @dagger.Module
    public static class Module {

        @Provides
        @PrecacheScope
        SplashPresenter provideSplashPresenter() {
            return new SplashPresenter();
        }
    }

    @dagger.Component(modules = Module.class)
    @PrecacheScope
    public interface Component {
        void inject(SplashActivity activity);
    }

    //endregion
}
