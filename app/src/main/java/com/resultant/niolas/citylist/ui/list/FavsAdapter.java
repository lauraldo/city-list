package com.resultant.niolas.citylist.ui.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.resultant.niolas.citylist.R;
import com.resultant.niolas.citylist.model.CityDto;
import com.resultant.niolas.citylist.ui.listener.FavListListener;

import java.util.List;

/**
 * Created by lauraldo on 10.01.17.
 */

public class FavsAdapter extends RecyclerView.Adapter<FavsAdapter.ViewHolder> {

    private List<CityDto> data;

    private FavListListener favListListener;

    public FavsAdapter(List<CityDto> favs) {
        data = favs;
    }

    public FavsAdapter(List<CityDto> favs, FavListListener listener) {
        data = favs;
        favListListener = listener;
    }

    public void refreshData(List<CityDto> favs) {
        data = favs;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_city, parent,
                false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CityDto item = data.get(position);
        holder.cityName.setText(item.name);
        holder.removeButton.setImageResource(R.drawable.ic_remove);
        if (favListListener != null) {
            holder.removeButton.setOnClickListener(view -> {
                favListListener.onRemoveFromFavs(item.id);
            });
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView cityName;
        private ImageView removeButton;

        public ViewHolder(View itemView) {
            super(itemView);
            cityName = (TextView) itemView.findViewById(R.id.cityName);
            removeButton = (ImageView) itemView.findViewById(R.id.favIcon);
        }
    }

}
