package com.resultant.niolas.citylist.network.res;

import com.squareup.moshi.Json;

import java.util.List;

/**
 * Created by lauraldo on 08.01.17.
 */

public class CityResponseMap {

    @Json(name = "Result")
    public List<Country> countries;

}
