package com.resultant.niolas.citylist.ui.list;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.resultant.niolas.citylist.databinding.FragmentListBinding;

/**
 * Created by lauraldo on 10.01.17.
 */

public class FavListFragment extends Fragment {

    private FragmentListBinding binding;

    private LinearLayoutManager layoutManager;

    //region ================================== Lifecycle ==================================

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentListBinding.inflate(inflater, container, false);
        layoutManager = new LinearLayoutManager(getMainActivity());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getMainActivity().setFavsAttached(true);
    }

    @Override
    public void onDestroyView() {
        getMainActivity().setFavsAttached(false);
        super.onDestroyView();
    }

    //endregion

    private MainActivity getMainActivity() {
        if (!(getActivity() instanceof MainActivity)) {
            throw new IllegalStateException("Parent activity should be MainActivity");
        }
        return (MainActivity) getActivity();
    }

    public void setAdapter(FavsAdapter adapter) {
        binding.cityList.setLayoutManager(layoutManager);
        binding.cityList.addItemDecoration(new DividerItemDecoration(getMainActivity(),
                DividerItemDecoration.VERTICAL));
        binding.cityList.setAdapter(adapter);
    }

    public FavsAdapter getAdapter() {
        return (FavsAdapter) binding.cityList.getAdapter();
    }
}
