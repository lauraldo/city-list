package com.resultant.niolas.citylist.network.res;

import com.squareup.moshi.Json;

/**
 * Created by lauraldo on 08.01.17.
 */

public class City {

    @Json(name = "Id")
    public int id;

    @Json(name = "CountryId")
    public int countryId;

    @Json(name = "Name")
    public String name;

}
