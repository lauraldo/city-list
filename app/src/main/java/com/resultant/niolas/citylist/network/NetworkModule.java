package com.resultant.niolas.citylist.network;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lauraldo on 08.01.17.
 */

@Module
public class NetworkModule {

    @Provides
    @NetworkScope
    CityListClient provideCityListClient() {
        return ServiceGenerator.createService(CityListClient.class);
    }

}
