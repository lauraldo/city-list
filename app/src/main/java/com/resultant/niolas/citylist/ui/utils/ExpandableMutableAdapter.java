package com.resultant.niolas.citylist.ui.utils;

import com.thoughtbot.expandablerecyclerview.ExpandCollapseController;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.listeners.GroupExpandCollapseListener;
import com.thoughtbot.expandablerecyclerview.listeners.OnGroupClickListener;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.models.ExpandableList;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import java.util.List;

/**
 * Created by lauraldo on 10.01.17.
 */

public abstract class ExpandableMutableAdapter<GVH extends GroupViewHolder, CVH extends ChildViewHolder> extends ExpandableRecyclerViewAdapter {

    private ExpandCollapseController collapseController;
    private OnGroupClickListener groupClickListener;
    private GroupExpandCollapseListener expandListener;

    public ExpandableMutableAdapter(List<? extends ExpandableGroup> groups) {
        super(groups);
        this.collapseController = new ExpandCollapseController(expandableList, this);
    }

    public void setGroups(List<? extends ExpandableGroup> groups) {
        this.expandableList = new ExpandableList(groups);
        this.collapseController = new ExpandCollapseController(expandableList, this);
    }

    @Override
    public void setOnGroupClickListener(OnGroupClickListener listener) {
        groupClickListener = listener;
    }

    @Override
    public void setOnGroupExpandCollapseListener(GroupExpandCollapseListener listener) {
        expandListener = listener;
    }

    @Override
    public boolean onGroupClick(int flatPos) {
        if (groupClickListener != null) {
            groupClickListener.onGroupClick(flatPos);
        }
        return collapseController.toggleGroup(flatPos);
    }

    @Override
    public boolean toggleGroup(int flatPos) {
        return collapseController.toggleGroup(flatPos);
    }

    @Override
    public boolean toggleGroup(ExpandableGroup group) {
        return collapseController.toggleGroup(group);
    }

    @Override
    public boolean isGroupExpanded(int flatPos) {
        return collapseController.isGroupExpanded(flatPos);
    }

    @Override
    public boolean isGroupExpanded(ExpandableGroup group) {
        return collapseController.isGroupExpanded(group);
    }
}
