package com.resultant.niolas.citylist.model;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/**
 * Created by lauraldo on 08.01.17.
 */

public class CountryDto extends ExpandableGroup<CityDto> {

    public int id;

    public String name;

    public CountryDto(String title, List<CityDto> items) {
        super(title, items);
    }
}
