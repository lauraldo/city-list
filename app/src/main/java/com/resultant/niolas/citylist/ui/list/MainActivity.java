package com.resultant.niolas.citylist.ui.list;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.resultant.niolas.citylist.R;
import com.resultant.niolas.citylist.core.di.DaggerService;
import com.resultant.niolas.citylist.core.view.BaseActivity;
import com.resultant.niolas.citylist.databinding.ActivityMainBinding;
import com.resultant.niolas.citylist.model.CityDto;
import com.resultant.niolas.citylist.model.CountryDto;
import com.resultant.niolas.citylist.ui.listener.CityListListener;
import com.resultant.niolas.citylist.ui.listener.FavListListener;

import java.util.List;

import javax.inject.Inject;

import dagger.Provides;

public class MainActivity extends BaseActivity implements ICommonView, CityListListener, FavListListener{

    private static final int TAB_COUNTRIES = 0;
    private static final int TAB_FAVS = 1;
    private static final int TAB_SETTINGS = 2;

    private ActivityMainBinding binding;

    private SectionsPagerAdapter sectionsPagerAdapter;

    private boolean citiesAttached = false, favsAttached = false;

    @Inject
    CommonPresenter presenter;

    //region ================================== Lifecycle ==================================

    @Override
    protected void onCreate(final Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setBinding(DataBindingUtil.setContentView(this, R.layout.activity_main));
        binding = (ActivityMainBinding) getBinding();

        DaggerService.getComponent(Component.class, new Module()).inject(this);

        setSupportActionBar(binding.toolbar);
        sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        binding.container.setAdapter(sectionsPagerAdapter);
        binding.tabs.setupWithViewPager(binding.container);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.attachView(this);
    }

    @Override
    protected void onStop() {
        presenter.detachView();
        super.onStop();
    }

    //endregion

    //region ================================== IView ==================================

    @Override
    public void showProgress(String progressMessage) {
        if (getSupportFragmentManager().getFragments() != null
                && !getSupportFragmentManager().getFragments().isEmpty()) {
            CountryListFragment fragment = (CountryListFragment) getSupportFragmentManager()
                    .getFragments().get(0);
            fragment.setRefreshing(true);
        }
    }

    @Override
    public void hideProgress() {
        if (getSupportFragmentManager().getFragments() != null
                && !getSupportFragmentManager().getFragments().isEmpty()) {
            CountryListFragment fragment = (CountryListFragment) getSupportFragmentManager()
                    .getFragments().get(0);
            fragment.setRefreshing(false);
        }
    }

    //endregion

    //region ================================== ICommonView ==================================

    @Override
    public void showListFromCache(List<CountryDto> data, int selectedCityId) {
//        CountryListFragment fragment = (CountryListFragment) sectionsPagerAdapter.getItem(0);
        if (getSupportFragmentManager().getFragments() != null && getSupportFragmentManager().getFragments().size() >= 2) {
            CountryListFragment fragment = (CountryListFragment) getSupportFragmentManager()
                    .getFragments().get(0);
            CountriesAdapter adapter = fragment.getAdapter();
            if (adapter == null) {
                fragment.setAdapter(new CountriesAdapter(data, this, selectedCityId));
            } else {
                adapter.refreshData(data, selectedCityId);
            }
        }
    }

    @Override
    public void showFavsFromCache(List<CityDto> favs) {
//        FavListFragment fragment = (FavListFragment) sectionsPagerAdapter.getItem(1);
        if (getSupportFragmentManager().getFragments() != null && getSupportFragmentManager().getFragments().size() >= 2) {
            FavListFragment fragment = (FavListFragment) getSupportFragmentManager()
                    .getFragments().get(1);
            FavsAdapter adapter = fragment.getAdapter();
            if (adapter == null) {
                fragment.setAdapter(new FavsAdapter(favs, this));
            } else {
                adapter.refreshData(favs);
            }
        }
    }

    //endregion

    //region ================================== ICityListListener ==================================

    @Override
    public void onCitySelected(int cityId) {
        presenter.makeCitySelected(cityId);
    }

    @Override
    public void onCityFavStateChanged(int cityId, boolean isFav) {
        if (isFav) {
            presenter.makeCityFav(cityId);
        } else {
            presenter.removeCityFromFav(cityId);
        }
    }

    //endregion

    //region ================================== IFavListListener ==================================

    @Override
    public void onRemoveFromFavs(int cityId) {
        presenter.removeCityFromFav(cityId);
    }

    //endregion

    public void setCitiesAttached(boolean citiesAttached) {
        this.citiesAttached = citiesAttached;
        if (citiesAttached && favsAttached) {
            presenter.initView();
        }
    }

    public void setFavsAttached(boolean favsAttached) {
        this.favsAttached = favsAttached;
        if (citiesAttached && favsAttached) {
            presenter.initView();
        }
    }

    public void swipeRefreshPerformed() {
        presenter.refreshCitiesInfo();
    }

    //region ================================== DI ==================================

    @dagger.Module
    public static class Module {

        @Provides
        @ListScope
        CommonPresenter provideCommonPresenter() {
            return new CommonPresenter();
        }
    }

    @dagger.Component(modules = Module.class)
    @ListScope
    public interface Component {
        void inject(MainActivity activity);
    }

    //endregion

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case TAB_COUNTRIES:
                    return new CountryListFragment();
                case TAB_FAVS:
                    return new FavListFragment();
                case TAB_SETTINGS:
                    return new SettingsFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case TAB_COUNTRIES:
                    return getString(R.string.countries);
                case TAB_FAVS:
                    return getString(R.string.favourites);
                case TAB_SETTINGS:
                    return getString(R.string.settings);
            }
            return null;
        }
    }

}
