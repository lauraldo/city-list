package com.resultant.niolas.citylist;

import android.app.Application;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

import com.activeandroid.ActiveAndroid;
import com.resultant.niolas.citylist.core.di.AppModule;
import com.resultant.niolas.citylist.core.di.DaggerAppComponent;
import com.resultant.niolas.citylist.core.di.DaggerService;

import java.io.File;

/**
 * Created by lauraldo on 08.01.17.
 */

public class CityListApp extends Application {

    public static final String CACHE_PATH = "//data//%s//databases//citylist.cache";

    private void initDatabase() {

        File db_file = new File(Environment.getDataDirectory(), String.format(CACHE_PATH,
                getPackageName()));
        if (db_file.exists()) {
            SQLiteDatabase db = SQLiteDatabase.openDatabase(db_file.getAbsolutePath(), null,
                    SQLiteDatabase.OPEN_READONLY);
            try {
                int fileVersion = db.getVersion();
                db.close();
                int newVersion = getPackageManager().getApplicationInfo(getPackageName(),
                        PackageManager.GET_META_DATA).metaData.getInt("AA_DB_VERSION");
                if (fileVersion < newVersion) {
                    db_file.delete();
                }
            } catch (PackageManager.NameNotFoundException e) {
                db_file.delete();
            }
        }

        ActiveAndroid.initialize(this);
    }

    public void initDagger() {
        DaggerService.initAppComponent(DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build());
    }

    @Override
    public void onCreate() {
        initDatabase();
        super.onCreate();
        initDagger();
    }
}
