package com.resultant.niolas.citylist.ui.list;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.resultant.niolas.citylist.R;
import com.resultant.niolas.citylist.model.CityDto;
import com.resultant.niolas.citylist.model.CountryDto;
import com.resultant.niolas.citylist.ui.listener.CityListListener;
import com.resultant.niolas.citylist.ui.utils.ExpandableMutableAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import java.util.List;

/**
 * Created by lauraldo on 09.01.17.
 */

public class CountriesAdapter extends ExpandableMutableAdapter<CountriesAdapter.CountryViewHolder, CountriesAdapter.CityViewHolder> {

    private CityListListener cityListListener;
    private static boolean[] oldIndices;
    private int selectedId = -1;

    public CountriesAdapter(List<? extends ExpandableGroup> groups) {
        super(groups);
    }

    public CountriesAdapter(List<? extends ExpandableGroup> groups, CityListListener listener, int selectedId) {
        this(groups);
        this.selectedId = selectedId;
        cityListListener = listener;
        if ((oldIndices != null) && (oldIndices.length > 0)) {
            expandableList.expandedGroupIndexes = oldIndices;
        }
    }

    public void refreshData(List<CountryDto> data, int selectedCity) {
        oldIndices = expandableList.expandedGroupIndexes;
        this.selectedId = selectedCity;
        setGroups(data);
        notifyDataSetChanged();
        expandableList.expandedGroupIndexes = oldIndices;
    }

    @Override
    public CountryViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_country, parent, false);
        return new CountryViewHolder(v);
    }

    @Override
    public CityViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_city, parent, false);
        return new CityViewHolder(v);
    }

    @Override
    public void onBindChildViewHolder(ChildViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        CityViewHolder cityViewHolder = (CityViewHolder) holder;
        CityDto item = (CityDto) group.getItems().get(childIndex);
        cityViewHolder.onBind(item);
    }

    @Override
    public void onBindGroupViewHolder(GroupViewHolder holder, int flatPosition, ExpandableGroup group) {
        CountryViewHolder countryViewHolder = (CountryViewHolder) holder;
        countryViewHolder.setCountryName(group);
    }

    @Override
    public void onGroupExpanded(int positionStart, int itemCount) {
        super.onGroupExpanded(positionStart, itemCount);
        oldIndices = expandableList.expandedGroupIndexes;
    }

    @Override
    public void onGroupCollapsed(int positionStart, int itemCount) {
        super.onGroupCollapsed(positionStart, itemCount);
        oldIndices = expandableList.expandedGroupIndexes;
    }

    public class CountryViewHolder extends GroupViewHolder {

        private TextView countryName;

        public CountryViewHolder(View itemView) {
            super(itemView);
            countryName = (TextView) itemView.findViewById(R.id.countryName);
        }

        public void setCountryName(ExpandableGroup group) {
            countryName.setText(group.getTitle());
        }

    }

    public class CityViewHolder extends ChildViewHolder {

        private TextView cityName;
        private ImageView favFlag;

        public CityViewHolder(View itemView) {
            super(itemView);
            cityName = (TextView) itemView.findViewById(R.id.cityName);
            favFlag = (ImageView) itemView.findViewById(R.id.favIcon);
        }

        public void onBind(CityDto city) {
            itemView.setBackgroundResource(city.id == selectedId
                    ? R.drawable.alpha_back_selected
                    : R.drawable.alpha_back);
            cityName.setText(city.name);
            favFlag.setImageResource(city.isFav
                    ? R.drawable.ic_fav_active
                    : R.drawable.ic_fav_inactive);
            if (cityListListener != null) {
                itemView.setOnClickListener(view -> {
                    cityListListener.onCitySelected(city.id);
                });
            }
            favFlag.setOnClickListener(view -> {
                cityListListener.onCityFavStateChanged(city.id, !city.isFav);
            });
        }

    }

}
