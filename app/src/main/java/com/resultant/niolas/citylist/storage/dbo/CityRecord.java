package com.resultant.niolas.citylist.storage.dbo;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.resultant.niolas.citylist.network.res.City;

/**
 * Created by lauraldo on 08.01.17.
 */

@SuppressWarnings("WeakerAccess")
@Table(name = "city")
public class CityRecord extends Model {

    @Column(name = "cid", index = true)
    public int id;

    @Column(name = "country_id")
    public int countryId;

    @Column
    public String name;

    @Column
    public boolean fav;

    public CityRecord() {
        super();
    }

    public CityRecord(City city) {
        super();
        this.id = city.id;
        this.countryId = city.countryId;
        this.name = city.name;
        this.fav = false;
    }

    public void update(City city) {
        this.countryId = city.countryId;
        this.name = city.name;
        this.fav = false;
    }

}
