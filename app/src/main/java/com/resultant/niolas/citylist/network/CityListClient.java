package com.resultant.niolas.citylist.network;

import com.resultant.niolas.citylist.network.res.CityResponseMap;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by lauraldo on 08.01.17.
 */

public interface CityListClient {

    @GET("countries")
    Observable<CityResponseMap> getCityList();

}
