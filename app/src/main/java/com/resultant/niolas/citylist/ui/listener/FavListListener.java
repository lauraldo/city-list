package com.resultant.niolas.citylist.ui.listener;

/**
 * Created by lauraldo on 10.01.17.
 */

public interface FavListListener {

    void onRemoveFromFavs(int cityId);

}
