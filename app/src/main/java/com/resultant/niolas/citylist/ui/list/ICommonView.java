package com.resultant.niolas.citylist.ui.list;

import com.resultant.niolas.citylist.core.view.IView;
import com.resultant.niolas.citylist.model.CityDto;
import com.resultant.niolas.citylist.model.CountryDto;

import java.util.List;

/**
 * Created by lauraldo on 10.01.17.
 */

public interface ICommonView extends IView {

    void showListFromCache(List<CountryDto> data, int selectedCityId);

    void showFavsFromCache(List<CityDto> favs);
}
