package com.resultant.niolas.citylist.ui.splash;

import com.resultant.niolas.citylist.core.di.DaggerService;
import com.resultant.niolas.citylist.network.CityListClient;
import com.resultant.niolas.citylist.network.NetworkModule;
import com.resultant.niolas.citylist.network.NetworkScope;
import com.resultant.niolas.citylist.network.res.Country;
import com.resultant.niolas.citylist.storage.DataManager;
import com.resultant.niolas.citylist.storage.StorageModule;
import com.resultant.niolas.citylist.storage.StorageScope;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by lauraldo on 09.01.17.
 */

public class SplashModel {

    private ISplashPresenter presenter;

    @Inject
    DataManager dataManager;

    @Inject
    CityListClient cityListClient;

    @Inject
    SplashModel() {
    }

    SplashModel(ISplashPresenter presenter) {
        DaggerService.getComponent(Component.class, new StorageModule(), new NetworkModule())
                .inject(this);
        this.presenter = presenter;
    }

    public void storeCitiesInfo(List<Country> countries) {
        dataManager.setCountryData(countries);
    }

    public void requestCitiesInfo() {
        presenter.citiesLoaded(cityListClient.getCityList());
    }

    public boolean dataStored() {
        return dataManager.isDataCached();
    }

    //region ================================== DI ==================================

    @dagger.Component(modules = {StorageModule.class, NetworkModule.class})
    @PrecacheScope
    @NetworkScope
    @StorageScope
    public interface Component {
        void inject(SplashModel model);
    }

    //endregion

}