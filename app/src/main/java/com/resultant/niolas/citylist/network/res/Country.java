package com.resultant.niolas.citylist.network.res;

import com.squareup.moshi.Json;

import java.util.List;

/**
 * Created by lauraldo on 08.01.17.
 */

public class Country {

    @Json(name = "Id")
    public int id;

    @Json(name = "Name")
    public String name;

    @Json(name = "Cities")
    public List<City> cities;

}
