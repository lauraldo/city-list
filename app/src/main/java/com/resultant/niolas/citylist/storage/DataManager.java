package com.resultant.niolas.citylist.storage;

import android.content.Context;

import com.resultant.niolas.citylist.core.di.AppComponent;
import com.resultant.niolas.citylist.core.di.DaggerService;
import com.resultant.niolas.citylist.network.res.Country;
import com.resultant.niolas.citylist.storage.dbo.CityRecord;
import com.resultant.niolas.citylist.storage.dbo.CountryRecord;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Observable;

/**
 * Created by lauraldo on 08.01.17.
 */

public class DataManager {

    @Inject
    Context context;

    @Inject
    SharedPrefsManager sharedPrefsManager;

    @Inject
    DatabaseHelper databaseHelper;

    @Inject
    public DataManager() {
        AppComponent appComponent = DaggerService.getComponent(AppComponent.class);
        DaggerService.getComponent(Component.class, appComponent, new StorageModule()).inject(this);
    }

    public void setSelectedCity(int cityId) {
        sharedPrefsManager.saveSelectedCity(cityId);
    }

    public int getSelectedCity() {
        return sharedPrefsManager.loadSelectedCity();
    }

    public Completable setCountryData(List<Country> countries) {
        return databaseHelper.saveCountryData(countries);
    }

    public Observable<List<CountryRecord>> getCountryData() {
        return databaseHelper.loadCountryData();
    }

    public List<CityRecord> getCitiesByCountry(int countryId) {
        return databaseHelper.loadCities(countryId);
    }

    public Observable<List<CityRecord>> getFavCities() {
        return databaseHelper.loadFavCities();
    }

    public void markCityAsFav(int cityId) {
        databaseHelper.setCityFavFlag(cityId, true);
    }

    public void removeCityFromFavs(int cityId) {
        databaseHelper.setCityFavFlag(cityId, false);
    }

    public boolean isDataCached() {
        return databaseHelper.dataStored();
    }

    //region ================================== DI ==================================

    @dagger.Component(dependencies = AppComponent.class, modules = StorageModule.class)
    @StorageScope
    public interface Component {
        void inject(DataManager dataManager);
    }

    //endregion

}
