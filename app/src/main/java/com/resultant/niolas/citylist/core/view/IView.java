package com.resultant.niolas.citylist.core.view;

/**
 * Created by lauraldo on 08.01.17.
 */

public interface IView {

    void showMessage(String message);

    void showError(Throwable throwable);

    void showProgress(String progressMessage);

    void hideProgress();

}
