package com.resultant.niolas.citylist.storage.dbo;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.resultant.niolas.citylist.network.res.Country;

/**
 * Created by lauraldo on 08.01.17.
 */

@SuppressWarnings("WeakerAccess")
@Table(name = "country")
public class CountryRecord extends Model {

    @Column(name = "cnid", index = true)
    public int id;

    @Column
    public String name;

    public CountryRecord() {
        super();
    }

    public CountryRecord(Country country) {
        super();
        this.id = country.id;
        this.name = country.name;
    }

    public void update(Country country) {
        this.name = country.name;
    }

}