package com.resultant.niolas.citylist.network.exception;

/**
 * Created by lauraldo on 11.01.17.
 */

public class RemoteConnectionException extends Exception {

    private String message;

    public RemoteConnectionException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
