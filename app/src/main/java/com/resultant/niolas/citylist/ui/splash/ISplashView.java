package com.resultant.niolas.citylist.ui.splash;

import com.resultant.niolas.citylist.core.view.IView;

/**
 * Created by lauraldo on 08.01.17.
 */

public interface ISplashView extends IView {

    void proceedToMainScreen();
}
