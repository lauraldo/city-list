package com.resultant.niolas.citylist.ui.list;

import com.resultant.niolas.citylist.network.res.CityResponseMap;

import io.reactivex.Observable;

/**
 * Created by lauraldo on 10.01.17.
 */

public interface ICommonPresenter {

    void refreshCitiesInfo();

    void citiesLoaded(Observable<CityResponseMap> data);

    void makeCitySelected(int cityId);

    void makeCityFav(int cityId);

    void removeCityFromFav(int cityId);

}
