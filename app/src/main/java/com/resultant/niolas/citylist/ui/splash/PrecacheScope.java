package com.resultant.niolas.citylist.ui.splash;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by lauraldo on 09.01.17.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PrecacheScope {
}
