package com.resultant.niolas.citylist.ui.splash;

import com.resultant.niolas.citylist.network.res.CityResponseMap;

import io.reactivex.Observable;

/**
 * Created by lauraldo on 09.01.17.
 */

public interface ISplashPresenter {

    void loadCitiesInfo();

    void citiesLoaded(Observable<CityResponseMap> data);

}
