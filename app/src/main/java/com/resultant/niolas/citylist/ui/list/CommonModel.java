package com.resultant.niolas.citylist.ui.list;

import com.resultant.niolas.citylist.core.di.DaggerService;
import com.resultant.niolas.citylist.network.CityListClient;
import com.resultant.niolas.citylist.network.NetworkModule;
import com.resultant.niolas.citylist.network.NetworkScope;
import com.resultant.niolas.citylist.network.res.Country;
import com.resultant.niolas.citylist.storage.DataManager;
import com.resultant.niolas.citylist.storage.StorageModule;
import com.resultant.niolas.citylist.storage.StorageScope;
import com.resultant.niolas.citylist.storage.dbo.CityRecord;
import com.resultant.niolas.citylist.storage.dbo.CountryRecord;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by lauraldo on 10.01.17.
 */

public class CommonModel {

    private ICommonPresenter presenter;

    @Inject
    DataManager dataManager;

    @Inject
    CityListClient cityListClient;

    @Inject
    CommonModel() {

    }

    CommonModel(ICommonPresenter presenter) {
        DaggerService.getComponent(Component.class, new StorageModule(), new NetworkModule())
                .inject(this);
        this.presenter = presenter;
    }

    public void storeCitiesInfo(List<Country> countries) {
        dataManager.setCountryData(countries);
    }

    public void requestCitiesInfo() {
        presenter.citiesLoaded(cityListClient.getCityList());
    }

    public Observable<List<CountryRecord>> getCitiesInfo() {
        return dataManager.getCountryData();
    }

    public List<CityRecord> getCitiesByCountry(int countryId) {
        return dataManager.getCitiesByCountry(countryId);
    }

    public Observable<List<CityRecord>> getFavCities() {
        return dataManager.getFavCities();
    }

    public void addCityToFavs(int cityId) {
        dataManager.markCityAsFav(cityId);
    }

    public void removeCityFromFavs(int cityId) {
        dataManager.removeCityFromFavs(cityId);
    }

    public void setSelectedCity(int cityId) {
        dataManager.setSelectedCity(cityId);
    }

    public int getSelectedCity() {
        return dataManager.getSelectedCity();
    }

    //region ================================== DI ==================================

    @dagger.Component(modules = {StorageModule.class, NetworkModule.class})
    @ListScope
    @NetworkScope
    @StorageScope
    public interface Component {
        void inject(CommonModel model);
    }

    //endregion

}
