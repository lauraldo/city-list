package com.resultant.niolas.citylist.core.adapter;

import com.resultant.niolas.citylist.model.CountryDto;
import com.resultant.niolas.citylist.storage.dbo.CountryRecord;

/**
 * Created by lauraldo on 09.01.17.
 */

public class CountryAdapter {

    public static CountryDto fromDbo(CountryRecord record) {
        CountryDto result = new CountryDto(record.name, null);
        result.id = record.id;
        result.name = record.name;
        return result;
    }

    public static CountryRecord toDbo(CountryDto dto) {
        CountryRecord record = new CountryRecord();
        record.id = dto.id;
        record.name = dto.name;
        return record;
    }

}
