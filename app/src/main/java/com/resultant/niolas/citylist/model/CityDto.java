package com.resultant.niolas.citylist.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by lauraldo on 08.01.17.
 */

public class CityDto implements Parcelable {

    public int id;

    public int countryId;

    public String name;

    public boolean isFav;

    public CityDto() {

    }

    protected CityDto(Parcel in) {
        id = in.readInt();
        countryId = in.readInt();
        name = in.readString();
        isFav = in.readByte() != 0;
    }

    public static final Creator<CityDto> CREATOR = new Creator<CityDto>() {
        @Override
        public CityDto createFromParcel(Parcel in) {
            return new CityDto(in);
        }

        @Override
        public CityDto[] newArray(int size) {
            return new CityDto[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(countryId);
        parcel.writeString(name);
        parcel.writeByte((byte) (isFav ? 1 : 0));
    }
}
