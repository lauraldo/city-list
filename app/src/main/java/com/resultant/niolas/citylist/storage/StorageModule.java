package com.resultant.niolas.citylist.storage;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lauraldo on 08.01.17.
 */

@Module
public class StorageModule {

    @Provides
    @StorageScope
    SharedPrefsManager provideSharedPrefsManager() {
        return new SharedPrefsManager();
    }

    @Provides
    @StorageScope
    DatabaseHelper provideDatabaseHelper() {
        return new DatabaseHelper();
    }

    @Provides
    @StorageScope
    DataManager provideDataManager() {
        return new DataManager();
    }

}
