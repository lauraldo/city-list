package com.resultant.niolas.citylist.ui.listener;

/**
 * Created by lauraldo on 10.01.17.
 */

public interface CityListListener {

    void onCitySelected(int cityId);

    void onCityFavStateChanged(int cityId, boolean isFav);

}
