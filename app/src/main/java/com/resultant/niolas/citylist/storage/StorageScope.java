package com.resultant.niolas.citylist.storage;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by lauraldo on 08.01.17.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface StorageScope {
}
