package com.resultant.niolas.citylist.storage;

import android.database.sqlite.SQLiteException;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;
import com.resultant.niolas.citylist.network.res.City;
import com.resultant.niolas.citylist.network.res.Country;
import com.resultant.niolas.citylist.storage.dbo.CityRecord;
import com.resultant.niolas.citylist.storage.dbo.CountryRecord;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Observable;

/**
 * Created by lauraldo on 08.01.17.
 */

public class DatabaseHelper {

    @Inject
    public DatabaseHelper() {
    }

    private void clearCountries() {
        try {
            new Delete().from(CountryRecord.class).execute();
            new Delete().from(CityRecord.class).execute();
        } catch (SQLiteException ignored) {

        }
    }

    public boolean dataStored() {
        try {
            int countryCount = new Select().from(CountryRecord.class).count();
            int cityCount = new Select().from(CityRecord.class).count();
            return countryCount > 0 && cityCount > 0;
        } catch (SQLiteException e) {
            return false;
        }
    }

    public Completable saveCountryData(List<Country> countries) {
        ActiveAndroid.beginTransaction();
        clearCountries();
        try {
            for (Country country : countries) {
                CountryRecord record = new CountryRecord(country);
                record.save();
                if (country.cities != null) {
                    for (City city : country.cities) {
                        prepareCity(city).save();
                    }
                }
            }
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
        return Completable.complete();
    }

    public Observable<List<CountryRecord>> loadCountryData() {
        return Observable.just(new Select().from(CountryRecord.class).execute());
    }

    public List<CityRecord> loadCities(final int countryId) {
        return new Select()
                .from(CityRecord.class)
                .where("country_id = ?", countryId)
                .execute();
    }

    public Observable<List<CityRecord>> loadFavCities() {
        return Observable.just(new Select()
                .from(CityRecord.class)
                .where("fav = ?", true)
                .execute());
    }

    public void setCityFavFlag(int cityId, boolean isFav) {
        CityRecord record = new Select().from(CityRecord.class)
                .where("cid = ?", cityId)
                .executeSingle();
        if (record != null) {
            record.fav = isFav;
            record.save();
        }
    }

    private CityRecord prepareCity(City city) {
        if (city != null) {
            CityRecord record = new Select().from(CityRecord.class)
                    .where("cid = ?", city.id)
                    .executeSingle();
            if (record == null) {
                record = new CityRecord(city);
            } else {
                record.update(city);
            }
            return record;
        } else {
            return null;
        }
    }

}
