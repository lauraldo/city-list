package com.resultant.niolas.citylist.network.exception;

/**
 * Created by lauraldo on 11.01.17.
 */

public class UnknownException extends Exception {

    private String message;

    public UnknownException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
