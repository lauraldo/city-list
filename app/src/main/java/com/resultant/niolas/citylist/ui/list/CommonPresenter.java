package com.resultant.niolas.citylist.ui.list;

import com.resultant.niolas.citylist.core.adapter.CityAdapter;
import com.resultant.niolas.citylist.core.di.DaggerService;
import com.resultant.niolas.citylist.core.presenter.Presenter;
import com.resultant.niolas.citylist.model.CityDto;
import com.resultant.niolas.citylist.model.CountryDto;
import com.resultant.niolas.citylist.model.DtoAggregator;
import com.resultant.niolas.citylist.network.exception.RemoteConnectionException;
import com.resultant.niolas.citylist.network.exception.UnknownException;
import com.resultant.niolas.citylist.network.res.CityResponseMap;
import com.resultant.niolas.citylist.storage.dbo.CityRecord;
import com.resultant.niolas.citylist.storage.dbo.CountryRecord;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.Provides;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by lauraldo on 10.01.17.
 */

public class CommonPresenter extends Presenter<ICommonView> implements ICommonPresenter {

    private static final String NO_CONNECTION = "Не удалось подключиться к серверу. Проверьте соединение.";
    private static final String UNKNOWN_ERROR = "Не удалось обновить данные. Попробуйте еще раз.";

    @Inject
    CommonModel model;

    @Inject
    CommonPresenter() {
        DaggerService.getComponent(Component.class, new Module(this)).inject(this);
    }

    //region ================================== Presenter ==================================

    @Override
    public void initView() {
        reloadFromCache();
    }

    //endregion

    private void reloadFromCache() {
        if (getView() != null) {
            Observable.zip(
                    model.getCitiesInfo()
                            .flatMap(countryRecords -> {
                                List<CountryDto> result = new ArrayList<>(countryRecords.size());
                                for (CountryRecord countryRecord : countryRecords) {
                                    List<CityRecord> cityRecords = model.getCitiesByCountry(countryRecord.id);
                                    List<CityDto> cityItems = new ArrayList<>();
                                    for (CityRecord cityRecord : cityRecords) {
                                        CityDto cityItem = CityAdapter.fromDbo(cityRecord);
                                        cityItems.add(cityItem);
                                    }
                                    CountryDto item = new CountryDto(countryRecord.name, cityItems);
                                    item.id = countryRecord.id;
                                    item.name = countryRecord.name;
                                    result.add(item);
                                }
                                return Observable.just(result);
                            }),
                    model.getFavCities(), (countryDtos, favRecords) -> {
                        DtoAggregator dtoAggregator = new DtoAggregator();
                        dtoAggregator.countries = countryDtos;
                        dtoAggregator.favs = new ArrayList<>(favRecords.size());
                        for (CityRecord record : favRecords) {
                            dtoAggregator.favs.add(CityAdapter.fromDbo(record));
                        }
                        return dtoAggregator;
                    })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(dtoAggregator -> {
                        getView().showListFromCache(dtoAggregator.countries, model.getSelectedCity());
                        getView().showFavsFromCache(dtoAggregator.favs);
                    }, throwable -> {
                        if (throwable instanceof UnknownHostException) {
                            getView().showError(new RemoteConnectionException(NO_CONNECTION));
                        } else {
                            getView().showError(new UnknownException(UNKNOWN_ERROR));
                        }
                    });
        }
    }

    //region ================================== ICommonPresenter ==================================

    @Override
    public void refreshCitiesInfo() {
        if (getView() != null) {
            getView().showProgress("Загрузка...");
            model.requestCitiesInfo();
        }
    }

    @Override
    public void citiesLoaded(Observable<CityResponseMap> data) {
        data
                .flatMap(cityResponseMap -> {
                    if ((cityResponseMap != null) && (cityResponseMap.countries != null)) {
                        model.storeCitiesInfo(cityResponseMap.countries);
                    }
                    return model.getCitiesInfo();
                })
                .flatMap(new Function<List<CountryRecord>, Observable<List<CountryDto>>>() {
                    @Override
                    public Observable<List<CountryDto>> apply(List<CountryRecord> countryRecords) throws Exception {
                        List<CountryDto> result = new ArrayList<>(countryRecords.size());
                        for (CountryRecord countryRecord : countryRecords) {
                            List<CityRecord> cityRecords = model.getCitiesByCountry(countryRecord.id);
                            List<CityDto> cityItems = new ArrayList<>();
                            for (CityRecord cityRecord : cityRecords) {
                                CityDto cityItem = CityAdapter.fromDbo(cityRecord);
                                cityItems.add(cityItem);
                            }
                            CountryDto item = new CountryDto(countryRecord.name, cityItems);
                            item.id = countryRecord.id;
                            item.name = countryRecord.name;
                            result.add(item);
                        }
                        return Observable.just(result);
                    }
                })
                .zipWith(model.getFavCities(), (countryDtos, favRecords) -> {
                    DtoAggregator dtoAggregator = new DtoAggregator();
                    dtoAggregator.countries = countryDtos;
                    dtoAggregator.favs = new ArrayList<>(favRecords.size());
                    for (CityRecord record : favRecords) {
                        dtoAggregator.favs.add(CityAdapter.fromDbo(record));
                    }
                    return dtoAggregator;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dtoAggregator -> {
                    getView().hideProgress();
                    model.setSelectedCity(-1); // clear selected city on refresh
                    dtoAggregator.favs.clear(); // clear favs on refresh
                    getView().showListFromCache(dtoAggregator.countries, model.getSelectedCity());
                    getView().showFavsFromCache(dtoAggregator.favs);
                }, throwable -> {
                    getView().hideProgress();
                    if (throwable instanceof UnknownHostException) {
                        getView().showError(new RemoteConnectionException(NO_CONNECTION));
                    } else {
                        getView().showError(new UnknownException(UNKNOWN_ERROR));
                    }
                });
    }

    @Override
    public void makeCitySelected(int cityId) {
        if (model.getSelectedCity() != cityId) {
            model.setSelectedCity(cityId);
            reloadFromCache();
        }
    }

    @Override
    public void makeCityFav(int cityId) {
        model.addCityToFavs(cityId);
        reloadFromCache();
    }

    @Override
    public void removeCityFromFav(int cityId) {
        model.removeCityFromFavs(cityId);
        reloadFromCache();
    }

    //endregion

    //region ================================== DI ==================================

    @dagger.Module
    public static class Module {

        private ICommonPresenter presenter;

        public Module(ICommonPresenter presenter) {
            this.presenter = presenter;
        }

        @Provides
        @ListScope
        CommonModel provideCommonModel() {
            return new CommonModel(presenter);
        }
    }

    @dagger.Component(modules = Module.class)
    @ListScope
    public interface Component {
        void inject(CommonPresenter presenter);
    }

    //endregion

}
