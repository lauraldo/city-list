package com.resultant.niolas.citylist.core.adapter;

import com.resultant.niolas.citylist.model.CityDto;
import com.resultant.niolas.citylist.storage.dbo.CityRecord;

/**
 * Created by lauraldo on 09.01.17.
 */

public class CityAdapter {

    public static CityDto fromDbo(CityRecord record) {
        CityDto result = new CityDto();
        result.id = record.id;
        result.countryId = record.countryId;
        result.name = record.name;
        result.isFav = record.fav;
        return result;
    }

    public static CityRecord toDbo(CityDto dto) {
        CityRecord record = new CityRecord();
        record.id = dto.id;
        record.countryId = dto.countryId;
        record.name = dto.name;
        record.fav = dto.isFav;
        return record;
    }

}
