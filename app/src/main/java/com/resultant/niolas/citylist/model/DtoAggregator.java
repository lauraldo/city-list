package com.resultant.niolas.citylist.model;

import java.util.List;

/**
 * Created by lauraldo on 10.01.17.
 */

public class DtoAggregator {

    public List<CountryDto> countries;

    public List<CityDto> favs;

}
