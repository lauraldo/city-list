package com.resultant.niolas.citylist.ui.splash;

import android.util.Log;

import com.resultant.niolas.citylist.core.di.DaggerService;
import com.resultant.niolas.citylist.core.presenter.Presenter;
import com.resultant.niolas.citylist.network.res.CityResponseMap;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import dagger.Provides;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by lauraldo on 09.01.17.
 */

public class SplashPresenter extends Presenter<ISplashView> implements ISplashPresenter {

    @Inject
    SplashModel model;

    @Inject
    SplashPresenter() {
        DaggerService.getComponent(Component.class, new Module(this)).inject(this);
    }

    //region ================================== ISplashPresenter ==================================

    @Override
    public void initView() {
        if (model.dataStored()) {
            Completable.timer(2, TimeUnit.SECONDS)
                    .subscribe(() -> {
                        Log.d("PRESENTER", "data stored");
                        if (getView() != null) {
                            getView().proceedToMainScreen();
                        }
                    });
        } else {
            loadCitiesInfo();
        }
    }

    @Override
    public void loadCitiesInfo() {
        model.requestCitiesInfo();
    }

    @Override
    public void citiesLoaded(Observable<CityResponseMap> citiesInfo) {

        citiesInfo
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cityResponseMap -> {
                    if ((cityResponseMap != null) && (cityResponseMap.countries != null)) {
                        model.storeCitiesInfo(cityResponseMap.countries);
                    }
                }, throwable -> {
                    if (getView() != null) {
                        getView().proceedToMainScreen();
                    }
                }, () -> {
                    if (getView() != null) {
                        getView().proceedToMainScreen();
                    }
                });
    }

    //endregion

    //region ================================== DI ==================================

    @dagger.Module
    public static class Module {

        private ISplashPresenter presenter;

        public Module(ISplashPresenter presenter) {
            this.presenter = presenter;
        }

        @Provides
        @PrecacheScope
        SplashModel provideSplashModel() {
            return new SplashModel(presenter);
        }
    }

    @dagger.Component(modules = Module.class)
    @PrecacheScope
    public interface Component {
        void inject(SplashPresenter presenter);
    }

    //endregion

}
