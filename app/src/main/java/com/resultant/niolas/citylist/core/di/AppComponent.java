package com.resultant.niolas.citylist.core.di;

import android.content.Context;

import com.resultant.niolas.citylist.storage.SharedPrefsManager;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by lauraldo on 08.01.17.
 */

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    Context getContext();

    void inject(SharedPrefsManager sharedPrefsManager);

}
