package com.resultant.niolas.citylist.core.presenter;

import com.resultant.niolas.citylist.core.view.IView;

/**
 * Created by lauraldo on 08.01.17.
 */

public abstract class Presenter<V extends IView> {

    private V view;

    public void attachView(V view) {
        this.view = view;
    }

    public abstract void initView();

    public void detachView() {
        view = null;
    }

    public V getView() {
        return view;
    }
}
